/* Test program to see what the new GtkTreeView is like */

#include <gtk/gtk.h>
#include <gnome-xml/tree.h>
#include <gnome-xml/parser.h>
#include <stdlib.h>
#include <unistd.h>

enum
{
  NODE_TYPE_COLUMN,
  NODE_CONTENTS_COLUMN,
  NODE_COLUMN_COUNT
};

static xmlDocPtr doc = NULL;

#define XB_TYPE_MODEL			(xb_model_get_type ())
#define XB_MODEL(obj)			(GTK_CHECK_CAST ((obj), XB_TYPE_MODEL, XbModel))
#define XB_MODEL_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), XB_TYPE_MODEL, XbModelClass))
#define XB_IS_MODEL(obj)		(GTK_CHECK_TYPE ((obj), XB_TYPE_MODEL))
#define XB_IS_MODEL_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((obj), XB_TYPE_MODEL))


typedef struct _XbModel       XbModel;
typedef struct _XbModelClass  XbModelClass;

struct _XbModel
{
  GtkObject parent;
  xmlDocPtr doc;
};

struct _XbModelClass
{
  GtkObjectClass parent_class;

  /* signals */
  /* Will be moved into the GtkTreeModelIface eventually */
  void       (* node_changed)         (GtkTreeModel *tree_model,
				       GtkTreePath  *path,
				       GtkTreeNode   node);
  void       (* node_inserted)        (GtkTreeModel *tree_model,
				       GtkTreePath  *path,
				       GtkTreeNode   node);
  void       (* node_child_toggled)   (GtkTreeModel *tree_model,
				       GtkTreePath  *path,
				       GtkTreeNode   node);
  void       (* node_deleted)         (GtkTreeModel *tree_model,
				       GtkTreePath  *path);
};


GtkType    xb_model_get_type           (void);
XbModel *xb_model_new                (void);

void       xb_model_node_changed       (XbModel *xbm,
                                        GtkTreePath    *path,
                                        GtkTreeNode     node);
void       xb_model_node_inserted      (XbModel *xbm,
                                        GtkTreePath    *path,
                                        GtkTreeNode     node);
void       xb_model_node_child_toggled (XbModel *xbm,
                                        GtkTreePath    *path,
                                        GtkTreeNode     node);
void       xb_model_node_deleted       (XbModel *xbm,
                                        GtkTreePath    *path);


void       xb_model_set_doc            (XbModel *xbm,
                                        xmlDocPtr doc);

int
main (int argc, char **argv)
{
  xmlNodePtr node;
  XbModel *model;
  GtkWidget *window;
  GtkWidget *sw;
  GtkWidget *treeview;
  GtkCellRenderer *renderer;
  GtkTreeViewColumn *column;
  
  if (argc < 2)
    {
      fprintf (stderr, "Specify an XML file on the command line\n");
      exit (1);
    }
  
  doc = xmlParseFile (argv[1]);

  if (doc == NULL || doc->root == NULL)
    {
      fprintf (stderr, "Failed to parse doc\n");
      exit (1);
    }

  gtk_init (&argc, &argv);
  
  model = xb_model_new ();
  xb_model_set_doc (model, doc);

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

  gtk_signal_connect (GTK_OBJECT (window),
                      "destroy",
                      GTK_SIGNAL_FUNC (gtk_main_quit),
                      NULL);
  
  sw = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
                                  GTK_POLICY_AUTOMATIC,
                                  GTK_POLICY_AUTOMATIC);

  gtk_container_add (GTK_CONTAINER (window), sw);

  treeview = gtk_tree_view_new_with_model (GTK_TREE_MODEL (model));

  renderer = gtk_cell_renderer_text_new ();

  column = gtk_tree_view_column_new_with_attributes ("Type",
                                                     renderer,
                                                     "text",
                                                     NODE_TYPE_COLUMN,
                                                     NULL);

  gtk_tree_view_append_column (GTK_TREE_VIEW (treeview), column);

  renderer = gtk_cell_renderer_text_new ();

  column = gtk_tree_view_column_new_with_attributes ("Contents",
                                                     renderer,
                                                     "text",
                                                     NODE_CONTENTS_COLUMN,
                                                     NULL);

  gtk_tree_view_append_column (GTK_TREE_VIEW (treeview), column);
  
  gtk_container_add (GTK_CONTAINER (sw), treeview);
  
  gtk_widget_show_all (window);
  
  gtk_main ();
  
  return 0;
}

#include <gtk/gtksignal.h>
#include <gtk/gtkmarshal.h>

enum {
  NODE_CHANGED,
  NODE_INSERTED,
  NODE_CHILD_TOGGLED,
  NODE_DELETED,
  
  LAST_SIGNAL
};

static void         xb_model_init                 (XbModel      *model_xbm);
static void         xb_model_class_init           (XbModelClass *class);
static void         xb_model_tree_model_init      (GtkTreeModelIface *iface);
static void         xb_model_finalize             (GObject *object);
static gint         xb_model_real_get_n_columns   (GtkTreeModel        *tree_model);
static GtkTreeNode  xb_model_real_get_node        (GtkTreeModel        *tree_model,
                                                        GtkTreePath         *path);
static GtkTreePath *xb_model_real_get_path        (GtkTreeModel        *tree_model,
                                                        GtkTreeNode          node);
static void         xb_model_real_node_get_value  (GtkTreeModel        *tree_model,
                                                        GtkTreeNode          node,
                                                        gint                 column,
                                                        GValue              *value);
static gboolean     xb_model_real_node_next       (GtkTreeModel        *tree_model,
                                                        GtkTreeNode         *node);
static GtkTreeNode  xb_model_real_node_children   (GtkTreeModel        *tree_model,
                                                        GtkTreeNode          node);
static gboolean     xb_model_real_node_has_child  (GtkTreeModel        *tree_model,
                                                        GtkTreeNode          node);
static gint         xb_model_real_node_n_children (GtkTreeModel        *tree_model,
                                                        GtkTreeNode          node);
static GtkTreeNode  xb_model_real_node_nth_child  (GtkTreeModel        *tree_model,
                                                        GtkTreeNode          node,
                                                        gint                 n);
static GtkTreeNode  xb_model_real_node_parent     (GtkTreeModel        *tree_model,
                                                        GtkTreeNode          node);



static guint model_xbm_signals[LAST_SIGNAL] = { 0 };
static gpointer parent_class;

GtkType
xb_model_get_type (void)
{
  static GtkType model_xbm_type = 0;

  if (!model_xbm_type)
    {
      static const GTypeInfo model_xbm_info =
      {
        sizeof (XbModelClass),
	NULL,		/* base_init */
	NULL,		/* base_finalize */
        (GClassInitFunc) xb_model_class_init,
	NULL,		/* class_finalize */
	NULL,		/* class_data */
        sizeof (XbModel),
	0,
        (GInstanceInitFunc) xb_model_init
      };

      static const GInterfaceInfo tree_model_info =
      {
	(GInterfaceInitFunc) xb_model_tree_model_init,
	NULL,
	NULL
      };

      model_xbm_type = g_type_register_static (GTK_TYPE_OBJECT,
                                               "XbModel",
                                               &model_xbm_info);
      g_type_add_interface_static (model_xbm_type,
				   GTK_TYPE_TREE_MODEL,
				   &tree_model_info);
    }

  return model_xbm_type;
}

XbModel*
xb_model_new (void)
{
  return XB_MODEL (gtk_type_new (XB_TYPE_MODEL));
}

static void
xb_model_class_init (XbModelClass *class)
{
  GtkObjectClass *object_class;

  object_class = (GtkObjectClass*) class;

  parent_class = g_type_class_peek_parent (class);
  
  model_xbm_signals[NODE_CHANGED] =
    gtk_signal_new ("node_changed",
                    GTK_RUN_FIRST,
                    GTK_CLASS_TYPE (object_class),
                    GTK_SIGNAL_OFFSET (XbModelClass, node_changed),
                    gtk_marshal_NONE__POINTER_POINTER,
                    GTK_TYPE_NONE, 2,
		    GTK_TYPE_POINTER,
		    GTK_TYPE_POINTER);
  model_xbm_signals[NODE_INSERTED] =
    gtk_signal_new ("node_inserted",
                    GTK_RUN_FIRST,
                    GTK_CLASS_TYPE (object_class),
                    GTK_SIGNAL_OFFSET (XbModelClass, node_inserted),
                    gtk_marshal_NONE__POINTER_POINTER,
                    GTK_TYPE_NONE, 2,
		    GTK_TYPE_POINTER,
		    GTK_TYPE_POINTER);
  model_xbm_signals[NODE_CHILD_TOGGLED] =
    gtk_signal_new ("node_child_toggled",
                    GTK_RUN_FIRST,
                    GTK_CLASS_TYPE (object_class),
                    GTK_SIGNAL_OFFSET (XbModelClass, node_child_toggled),
                    gtk_marshal_NONE__POINTER_POINTER,
                    GTK_TYPE_NONE, 2,
		    GTK_TYPE_POINTER,
		    GTK_TYPE_POINTER);
  model_xbm_signals[NODE_DELETED] =
    gtk_signal_new ("node_deleted",
                    GTK_RUN_FIRST,
                    GTK_CLASS_TYPE (object_class),
                    GTK_SIGNAL_OFFSET (XbModelClass, node_deleted),
                    gtk_marshal_NONE__POINTER,
                    GTK_TYPE_NONE, 1,
		    GTK_TYPE_POINTER);


  gtk_object_class_add_signals (object_class, model_xbm_signals, LAST_SIGNAL);

  G_OBJECT_CLASS (class)->finalize = xb_model_finalize;
}

static void
xb_model_tree_model_init (GtkTreeModelIface *iface)
{
  iface->get_n_columns = xb_model_real_get_n_columns;
  iface->get_node = xb_model_real_get_node;
  iface->get_path = xb_model_real_get_path;
  iface->node_get_value = xb_model_real_node_get_value;
  iface->node_next = xb_model_real_node_next;
  iface->node_children = xb_model_real_node_children;
  iface->node_has_child = xb_model_real_node_has_child;
  iface->node_n_children = xb_model_real_node_n_children;
  iface->node_nth_child = xb_model_real_node_nth_child;
  iface->node_parent = xb_model_real_node_parent;
}


static void
xb_model_init (XbModel *model_xbm)
{
}

static void
xb_model_finalize (GObject *object)
{
  xb_model_set_doc (XB_MODEL (object), NULL);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

void
xb_model_set_doc (XbModel *xbm,
                  xmlDocPtr doc)
{
  if (doc == xbm->doc)
    return;

  xmlFreeDoc (xbm->doc);
  
  xbm->doc = doc;
}


static gint
xb_model_real_get_n_columns (GtkTreeModel *tree_model)
{
  return NODE_COLUMN_COUNT;
}

static GtkTreeNode
xb_model_real_get_node (GtkTreeModel *tree_model,
                             GtkTreePath  *path)
{
  gint i;
  xmlNodePtr node;
  gint *indices = gtk_tree_path_get_indices (path);
  XbModel *xbm;

  xbm = XB_MODEL (tree_model);

  if (xbm->doc == NULL || xbm->doc->root == NULL)
    return NULL;
  
  node = xbm->doc->root;

  i = 0;
  while (i < gtk_tree_path_get_depth (path))
    {
      node = xb_model_real_node_nth_child (tree_model, node,
                                                indices[i]); 

      if (node == NULL)
	return NULL;
      
      ++i;
    }

  return (GtkTreeNode) node;
}

static GtkTreePath *
xb_model_real_get_path (GtkTreeModel *tree_model,
                        GtkTreeNode   tree_node)
{
  GtkTreePath *retval;
  xmlNodePtr tmp_node;
  xmlNodePtr node;
  XbModel *xbm;
  gint i;

  g_return_val_if_fail (tree_model != NULL, NULL);

  xbm = XB_MODEL (tree_model);

  if (xbm->doc == NULL)
    return NULL;

  node = tree_node;
  
  if (node == xbm->doc->root)
    return NULL;
  
  if (node->parent == xbm->doc->root)
    retval = gtk_tree_path_new ();
  else
    retval = xb_model_real_get_path (tree_model, node->parent);

  tmp_node = node->parent->childs;
  
  if (retval == NULL)
    return NULL;

  if (tmp_node == NULL)
    {
      gtk_tree_path_free (retval);
      return NULL;
    }

  i = 0;
  while (tmp_node != NULL)
    {
      if (tmp_node == node)
        break;

      tmp_node = tmp_node->next;
      i++;
    }
  
  if (tmp_node == NULL)
    {
      /* Hmm, this node didn't exist. */
      gtk_tree_path_free (retval);
      return NULL;
    }

  gtk_tree_path_append_index (retval, i);

  return retval;
}

static void
xb_model_real_node_get_value (GtkTreeModel *tree_model,
                                   GtkTreeNode   tree_node,
                                   gint          column,
                                   GValue       *value)
{
  xmlNodePtr node = tree_node;
  
  switch (column)
    {
    case NODE_TYPE_COLUMN:
      g_value_init (value, G_TYPE_STRING);
      switch (node->type)
        {
        case XML_ELEMENT_NODE:
          g_value_set_string (value, "ELEMENT_NODE");
          break;
        case XML_ATTRIBUTE_NODE:
          g_value_set_string (value, "ATTRIBUTE_NODE");
          break;
        case XML_TEXT_NODE:
          g_value_set_string (value, "TEXT_NODE");
          break;
        case XML_CDATA_SECTION_NODE:
          g_value_set_string (value, "CDATA_SECTION_NODE");
          break;
        case XML_ENTITY_REF_NODE:
          g_value_set_string (value, "ENTITY_REF_NODE");
          break;
        case XML_ENTITY_NODE:
          g_value_set_string (value, "ENTITY_NODE");
          break;
        case XML_PI_NODE:
          g_value_set_string (value, "PI_NODE");
          break;
        case XML_COMMENT_NODE:
          g_value_set_string (value, "COMMENT_NODE");
          break;
        case XML_DOCUMENT_NODE:
          g_value_set_string (value, "DOCUMENT_NODE");
          break;
        case XML_DOCUMENT_TYPE_NODE:
          g_value_set_string (value, "DOCUMENT_TYPE_NODE");
          break;
        case XML_DOCUMENT_FRAG_NODE:
          g_value_set_string (value, "DOCUMENT_FRAG_NODE");
          break;
        case XML_NOTATION_NODE:
          g_value_set_string (value, "NOTATION_NODE");
          break;
        case XML_HTML_DOCUMENT_NODE:
          g_value_set_string (value, "HTML_DOCUMENT_NODE");
          break;
#if 0
        case XML_DTD_NODE:
          g_value_set_string (value, "DTD_NODE");
          break;
        case XML_ELEMENT_DECL:
          g_value_set_string (value, "ELEMENT_DECL");
          break;
        case XML_ATTRIBUTE_DECL:
          g_value_set_string (value, "ATTRIBUTE_DECL");
          break;
        case XML_ENTITY_DECL:
          g_value_set_string (value, "ENTITY_DECL");
          break;
#endif
        default:
          g_value_set_string (value, "***UNKNOWN***");
          break;
        }
      break;

    case NODE_CONTENTS_COLUMN:
      {
        gchar *contents;
        
        g_value_init (value, G_TYPE_STRING);
        contents = xmlNodeGetContent (node);
        g_value_set_string (value, contents);
        free (contents);
      }
      break;

    default:
      g_assert_not_reached ();
      break;
    }
}

static gboolean
xb_model_real_node_next (GtkTreeModel  *tree_model,
                         GtkTreeNode   *tree_node)
{
  xmlNodePtr *node = tree_node;

  *tree_node = (*node)->next;
  
  return *tree_node != NULL;
}

static GtkTreeNode
xb_model_real_node_children (GtkTreeModel *tree_model,
                                  GtkTreeNode   tree_node)
{
  xmlNodePtr node = tree_node;

  return node->childs;
}

static gboolean
xb_model_real_node_has_child (GtkTreeModel *tree_model,
                                   GtkTreeNode   tree_node)
{
  xmlNodePtr node = tree_node;

  return node->childs != NULL;
}

static gint
xb_model_real_node_n_children (GtkTreeModel *tree_model,
                                    GtkTreeNode   tree_node)
{
  xmlNodePtr node = tree_node;
  int i;

  i = 0;
  node = node->childs;
  while (node != NULL)
    {
      node = node->next;
      ++i;
    }

  return i;
}

static GtkTreeNode
xb_model_real_node_nth_child (GtkTreeModel *tree_model,
                                   GtkTreeNode   tree_node,
                                   gint          n)
{
  xmlNodePtr node = tree_node;

  int j = 0;
  
  node = node->childs;
  while (node && j < n)
    {
      node = node->next;
      ++j;
    }

  return node;
}

static GtkTreeNode
xb_model_real_node_parent (GtkTreeModel *tree_model,
                           GtkTreeNode   tree_node)
{
  xmlNodePtr node = tree_node;

  return node->parent;
}

/* Public functions */
void
xb_model_node_changed (XbModel *xbm,
                       GtkTreePath    *path,
                       GtkTreeNode     node)
{
  g_return_if_fail (xbm != NULL);
  g_return_if_fail (XB_IS_MODEL (xbm));
  g_return_if_fail (path != NULL);

  gtk_signal_emit_by_name (GTK_OBJECT (xbm), "node_changed", path, node);
}

void
xb_model_node_inserted (XbModel *xbm,
                        GtkTreePath    *path,
                        GtkTreeNode     node)
{
  g_return_if_fail (xbm != NULL);
  g_return_if_fail (XB_IS_MODEL (xbm));
  g_return_if_fail (path != NULL);

  gtk_signal_emit_by_name (GTK_OBJECT (xbm), "node_inserted", path, node);
}

void
xb_model_node_child_toggled (XbModel *xbm,
                             GtkTreePath    *path,
                             GtkTreeNode     node)
{
  g_return_if_fail (xbm != NULL);
  g_return_if_fail (XB_IS_MODEL (xbm));
  g_return_if_fail (path != NULL);

  gtk_signal_emit_by_name (GTK_OBJECT (xbm), "node_child_toggled", path, node);
}

void
xb_model_node_deleted (XbModel *xbm,
                       GtkTreePath    *path,
                       GtkTreeNode     node)
{
  g_return_if_fail (xbm != NULL);
  g_return_if_fail (XB_IS_MODEL (xbm));
  g_return_if_fail (path != NULL);

  gtk_signal_emit_by_name (GTK_OBJECT (xbm), "node_deleted", path, node);
}

