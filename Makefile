
CC=gcc

all: xml-browser

xml-browser: xml-browser.c
	$(CC) -Wall -g `gtk-config --libs --cflags` `xml-config --cflags --libs` xml-browser.c -o xml-browser
